const pad =  (hex) => {
    return(hex.length === 1 ? "0" + hex : hex ) 
} //pad funktio korjaa yhden merkin mittaiset arvot lisäämällä nollan

module.exports = {
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16) //"2"
        const greenHex = green.toString(16)
        const blueHex = blue.toString(16)

        return pad(redHex) + pad(greenHex) + pad(blueHex) //ff0000 -> 20000
    } ,
    //hex to rgb
    hexToRgb: (color) => {
        var r = parseInt(color.substring(0,2),16);
        var g = parseInt(color.substring(2,4),16);
        var b = parseInt(color.substring(4,6),16);

        return ('rgb(' + r + ', ' + g + ', ' + b + ')');
    }
}
    

