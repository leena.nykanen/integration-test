Integraatiotestaus

- Kun monta kehittäjää ja ohjelma koostuu monesta osasta
- 4 metodia: 
    - Big bang: Laitetaan osat yhteen ja toivotaan, että toimii. EI kovin hyvä menetelmä. Virheiden paikannus hankalaa. 
    - Top Down: Pystytään paikantamaan ongelma, kun lähdetään päätasolta alemmas. Voidaan hahmottaa, mikä onkaan tarpeellista ja missä kohtaa. 
                Ei välttämättä kata täysin kaiken testaamista. 
    - Bottoms Up: Yksikkötestit (edellisellä kerralla luennoilla näitä), eli testataan että kaikki pikkuosat ohjelmassa toimivat. Teknologia 
                edellä mentäessä hyvä tapa. Kun toimii--> isompia kokonaisuuksia. Virheiden paikallistaminen helpompaa. 
                Prototyyppien luonti on hankalaa alkuvaiheessa, joten kokonaisuuden demoaminen voi olla hankalaa alussa. Kriittiset moduulit 
                testataan viimeisenä
    - Sandwich: Valitaan kulloinkin sopivin tapa, ns. hybridimalli kahdesta edellisestä. 

Savutestaus: Käytännössä testi, lähteekö sovellus käyntiin.
    - Kääntäjää käyttävien kielten kanssa savutestaus korostuisi (esim. C++, C#, C)