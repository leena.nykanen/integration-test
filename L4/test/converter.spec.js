// TDD - unit testing; test driven development

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color code converter", () => {
    describe("RGB to Hex conversion", () => {
        it("converts the basic colos", () => {
            const redHex = converter.rgbToHex(255, 0, 0); //ff0000
            const greenHex = converter.rgbToHex(0, 255, 0); //00ff00
            const blueHex = converter.rgbToHex(0, 0, 255); //0000ff
        
            expect(redHex).to.equal("ff0000")
            expect(greenHex).to.equal("00ff00")
            expect(blueHex).to.equal("0000ff")
            
        })
    }),
    //Kotitehtävä toiseen suuntaan
    describe("Hex to RGB conversion", () => {
        it("Convert basic colours", () => {
            const hexRed = converter.hexToRgb('ff0000')
            const hexGreen = converter.hexToRgb('00ff00')
            const hexBlue = converter.hexToRgb('0000ff')

            expect(hexRed).to.string(255, 0, 0) //voisi olla myös to.deep.equal
            expect(hexGreen).to.string(0, 255, 0)
            expect(hexBlue).to.string(0, 0, 255)
        })
    })
})