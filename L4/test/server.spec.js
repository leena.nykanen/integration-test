const expect = require('chai').expect
const request = require('request')
const app = require('../src/server')
const port = 3000

describe("Color code converter API", () => {
    before("start server", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening on localhost:${3000}`)
            done();
        })
    })
    describe("rgb to hex conversion", () => {
        const url = `http://localhost:${port}/rgb-to-hex?red=255&green=255&blue=255`

        it("Returns status code 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            })
        })
        it("returns the color in hex", (done) => {
            request(url, (error, response, body) => {
            expect(body).to.equal("ffffff");
            done();
            });      
        })
    })
    after("Close server", (done) => {
        server.close()
        done();
    })
})